﻿Option Explicit On

Imports System.Text.RegularExpressions

Module Мозги

    Public i As Long
    Public a() As String
    Public Пример As String
    Public Licennzed As Boolean = False

    Public Function Голов(ByVal Текст As String)
        Dim t As String
        'Текст = ОтчОтТекста(Текст) 'В будущем это должно будет быть в другом месте.
        Текст = ФиксСтр(Текст)
        Текст = ДопПров(Текст)
        t = Текст
        If СкобкиПров(Текст) = True Then
            If ПоискНеточн(Текст) = True Then
                Голов = Кальк(Скобки(t))
            Else
                Return "Проверьте знаки!"
            End If
        Else
            Return "Проверьте скобки!"
        End If
    End Function

    Private Sub Опер(ByVal знак As String, ByVal i As Long, ByVal n As Long, ByVal вид As Long)
        Dim yy As Long
        Dim nn As Long
        Dim nnn As Long
        Dim b As Double
        Dim c As Double

        Select Case вид
            Case 1
                nnn = n - 1

                c = a(i + 1)

                Select Case знак
                    Case "sin"
                        a(i) = Math.Sin(c)
                    Case "cos"
                        a(i) = Math.Cos(c)
                    Case "tg"
                        a(i) = Math.Tan(c)
                    Case "ln"
                        a(i) = Math.Log10(c)
                    Case "exp"
                        a(i) = Math.Exp(c)
                    Case "√"
                        a(i) = Math.Sqrt(c)

                End Select

                i = i + 1
                For yy = i To n
                    If yy <= nnn Then
                        a(yy) = a(yy + 1)
                        a(yy + 1) = "0"
                    End If

                    If yy = nnn + 1 Then a(yy) = "0"
                    'If yy = nn + 2 Then a(yy) = "0"
                Next yy

            Case 2
                nn = n - 2

                b = a(i - 1)
                c = a(i + 1)

                Select Case знак
                    Case "^"
                        a(i - 1) = b ^ c
                    Case "mod"
                        a(i - 1) = b Mod c
                    Case "*"
                        a(i - 1) = b * c
                    Case "/"
                        a(i - 1) = b / c
                    Case "+"
                        a(i - 1) = b + c
                    Case "-"
                        a(i - 1) = b - c

                End Select


                For yy = i To n
                    If yy <= nn Then
                        a(yy) = a(yy + 2)
                        a(yy + 2) = "0"
                    End If
                    If yy = nn + 1 Then a(yy) = "0"
                    If yy = nn + 2 Then a(yy) = "0"
                Next yy

        End Select
    End Sub

    Private Function Кальк(ByVal Данные As String) As Double
        Dim ii As Long
        Dim n As Long

        If Данные <> "" Then
            Данные = LCase(Данные)
            Данные = ПерПроб(Данные)
            n = Кол(Данные)
            a = Split(Данные)

            For ii = 0 To 2

                For i = 0 To n

                    If a(i) = "^" Then
                        Опер("^", i, n, 2)

                    End If


                    If 0 < InStr(a(i), "sin", CompareMethod.Binary) And Len(a(i)) = 3 Then

                        Опер("sin", i, n, 1)

                    End If

                    If 0 < InStr(a(i), "cos", CompareMethod.Binary) And Len(a(i)) = 3 Then
                        Опер("cos", i, n, 1)

                    End If

                    If 0 < InStr(a(i), "tg", CompareMethod.Binary) And Len(a(i)) = 2 Then
                        Опер("tg", i, n, 1)

                    End If

                    If 0 < InStr(a(i), "ln", CompareMethod.Binary) And Len(a(i)) = 2 Then
                        Опер("ln", i, n, 1)

                    End If

                    If 0 < InStr(a(i), "mod", CompareMethod.Binary) And Len(a(i)) = 3 Then
                        Опер("mod", i, n, 2)

                    End If

                    If 0 < InStr(a(i), "exp", CompareMethod.Binary) And Len(a(i)) = 3 Then
                        Опер("exp", i, n, 1)

                    End If

                    If 0 < InStr(a(i), "√", CompareMethod.Binary) And Len(a(i)) = 1 Then
                        Опер("√", i, n, 1)

                    End If

                Next

                For i = 0 To n

                    If a(i) = "*" Then
                        Опер("*", i, n, 2)


                    End If

                    If a(i) = "/" Then
                        Опер("/", i, n, 2)

                    End If

                Next i


                For i = 0 To n

                    If a(i) = "+" Then
                        Опер("+", i, n, 2)


                    End If

                    If a(i) = "-" Then
                        Опер("-", i, n, 2)

                    End If

                Next i


            Next ii

            If n <> 0 Then
                If n > 0 Then
                    If a(1) <> 0 Then
                        Кальк = 0
                    Else
                        Кальк = a(0)
                    End If
                End If
            Else
                Кальк = a(0)
            End If
        End If
    End Function

    Public Function ОтчОтТекста(ByVal Строка As String) As String
        Dim Тип As String = ""
        If Строка <> "" Then
            Dim r As New Regex("[^a-zA-ZА-Яа-яЁё]", RegexOptions.IgnoreCase)
            Dim input As String
            Dim mc As MatchCollection
            input = Строка
            mc = r.Matches(input)
            For i = 0 To mc.Count - 1
                Тип = Тип & mc.Item(i).Value
            Next
        End If
        ОтчОтТекста = Тип
    End Function

    Public Function ОтчОтЦифр(ByVal Строка As String)
        Dim Тип As String = ""
        If Строка <> "" Then
            Dim r As New Regex("[^0-9().,]", RegexOptions.IgnoreCase)
            Dim input As String
            Dim mc As MatchCollection
            input = Строка
            mc = r.Matches(input)
            For i = 0 To mc.Count - 1
                Тип = Тип & mc.Item(i).Value
            Next
        End If
        ОтчОтЦифр = Тип
    End Function

    Public Function ОтчОтЗнаков(ByVal Строка As String)
        Dim Тип As String = ""
        If Строка <> "" Then
            Dim r As New Regex("[^+*/()<>^;:.,№=%&?#$-]", RegexOptions.IgnoreCase)
            Dim input As String
            Dim mc As MatchCollection
            input = Строка
            mc = r.Matches(input)
            For i = 0 To mc.Count - 1
                Тип = Тип & mc.Item(i).Value
            Next
        End If
        ОтчОтЗнаков = Тип
    End Function

    Public Function ФиксСтр(ByVal s As String)
        s = Фикс(s)
        s = ПравПроб(s)
        s = ПерПроб(s)
        ФиксСтр = s
    End Function

    Public Function ДопПров(ByVal s As String)
        Dim Тип As String = ""
        Dim Т As Long
        s = LCase(s)
        a = Split(s)
        Т = Кол(s)

        For i = 0 To Т
            If 0 < InStr("sin", a(i), CompareMethod.Text) And Len(a(i)) = 3 Then
                a(i) = "$!№"
            End If
            If 0 < InStr("cos", a(i), CompareMethod.Text) And Len(a(i)) = 3 Then
                a(i) = "@0$"
            End If
            If 0 < InStr("tg", a(i), CompareMethod.Text) And Len(a(i)) = 2 Then
                a(i) = "^#@"
            End If
            If 0 < InStr("ln", a(i), CompareMethod.Text) And Len(a(i)) = 2 Then
                a(i) = "!№:"
            End If
            If 0 < InStr("mod", a(i), CompareMethod.Text) And Len(a(i)) = 3 Then
                a(i) = "#0#"
            End If
            If 0 < InStr("exp", a(i), CompareMethod.Text) And Len(a(i)) = 3 Then
                a(i) = "~&%"
            End If
        Next
        Тип = Join(a)
        Тип = ОтчОтТекста(Тип)
        Тип = Replace(Тип, "$!№", "sin")
        Тип = Replace(Тип, "@0$", "cos")
        Тип = Replace(Тип, "^#@", "tg")
        Тип = Replace(Тип, "!№:", "ln")
        Тип = Replace(Тип, "#0#", "mod")
        Тип = Replace(Тип, "~&%", "exp")

        ДопПров = Тип
    End Function

    Public Function СкобкиПров(ByVal s As String) As Boolean
        Dim rew As String
        Dim Остат As Long
        Dim Остат1 As Long
        Dim k As Long

        If s <> "" Then

            s = Replace(s, "( ", "(")
            s = Replace(s, " )", ")")
            s = ПерПроб(s)

            For k = 0 To Кол(s)
                rew = ""

                For i = 1 To Len(s)

                    rew = Mid(s, i, 1)

                    If rew = ")" Then
                        Остат1 = Остат1 + 1
                    End If

                    If rew = "(" Then
                        Остат = Остат + 1
                    End If

                Next i

            Next k

        Else

        End If

        If Остат = Остат1 Then СкобкиПров = True Else СкобкиПров = False

    End Function

    Public Function Скобки(ByVal s As String)
        Dim dsx As Double
        Dim rew As String
        Dim rew1 As String
        Dim rew12 As String
        Dim Остат As Long
        Dim СкобМод As Long
        Dim k As Long

        If s <> "" Then

            s = Replace(s, "( ", "(")
            s = Replace(s, " )", ")")
            s = ПерПроб(s)

            For k = 0 To Кол(s)
                rew = ""
                rew1 = ""
                rew12 = ""
                СкобМод = 0
                Остат = 0

                For i = 1 To Len(s)

                    rew = Mid(s, i, 1)

                    If rew = ")" Then
                        Остат = 2
                        СкобМод = 1
                    End If

                    If Остат = 1 Then
                        rew1 = rew1 & Mid(s, i, 1)
                    End If

                    If rew = "(" Then
                        If СкобМод <> 1 Then
                            Остат = 1
                            rew1 = ""
                        End If
                    End If

                Next i

                If rew1 <> "" Then
                    'rew1 = ПравПроб(rew1)
                    'rew1 = ПерПроб(rew1)
                    rew12 = rew1

                    dsx = Кальк(rew1)

                    s = Replace(s, "(" & rew12 & ")", dsx)

                    s = ПравПроб(s)

                End If

            Next k

        Else

            s = "0"

        End If

        Скобки = s
    End Function

    Public Function УдалОтв(ByVal Текст As String)
        Dim dfre As String
        For i = 1 To Len(Текст)
            dfre = Mid(Текст, i, 1)
            If dfre = "=" Then Текст = Microsoft.VisualBasic.Strings.Left(Текст, i - 2)
        Next i
        УдалОтв = Текст
    End Function

    Public Function ПерПроб(ByVal Текст As String)
        Текст = Trim(Текст)
        ПерПроб = Текст
    End Function

    Public Function ПравПроб(ByVal Текст As String)
        For i = 1 To CountSpace(Текст)
            Текст = Replace(Текст, "  ", " ")
        Next i
        ПравПроб = Текст
    End Function

    Public Function Фикс(ByVal Текст As String)
        Текст = Replace(Текст, "++", "+")
        Текст = Replace(Текст, "+++", "+")
        Текст = Replace(Текст, "//", "/")
        Текст = Replace(Текст, "///", "/")
        Текст = Replace(Текст, "**", "*")
        Текст = Replace(Текст, "***", "*")
        Текст = Replace(Текст, "^^", "^")
        Текст = Replace(Текст, "^^^", "^")
        'Разделение операторов пробелами

        Текст = Replace(Текст, "+", " + ")
        Текст = Replace(Текст, "-", " - ")
        Текст = Replace(Текст, "*", " * ")
        Текст = Replace(Текст, "/", " / ")
        Текст = Replace(Текст, "^", " ^ ")

        Текст = Replace(Текст, "sin", " sin ")
        Текст = Replace(Текст, "cos", " cos ")
        Текст = Replace(Текст, "tg", " tg ")
        Текст = Replace(Текст, "ln", " ln ")
        Текст = Replace(Текст, "√", " √ ")
        Текст = Replace(Текст, "mod", " mod ")
        Текст = Replace(Текст, "exp", " exp ")
        'Текст = Replace(Текст, "+ +", "+")


        'Правка работы отрицательных значений.
        Текст = ПравПроб(Текст)
        Текст = Replace(Текст, "+ - ", "+ -")
        Текст = Replace(Текст, "- - ", "- -")
        Текст = Replace(Текст, "* - ", "* -")
        Текст = Replace(Текст, "/ - ", "/ -")
        Текст = Replace(Текст, "^ - ", "^ -")
        Текст = Replace(Текст, "sin - ", "sin -")
        Текст = Replace(Текст, "cos - ", "cos -")
        Текст = Replace(Текст, "tg - ", "tg -")
        Текст = Replace(Текст, "ln - ", "ln -")
        Текст = Replace(Текст, "mod - ", "mod -")
        Текст = Replace(Текст, "exp - ", "exp -")

        'sss
        Текст = Replace(Текст, " , ", ",")
        Текст = Replace(Текст, ", ", ",")
        Текст = Replace(Текст, " ,", ",")
        Текст = Replace(Текст, " . ", ".")
        Текст = Replace(Текст, ". ", ".")
        Текст = Replace(Текст, " .", ".")
        Текст = Replace(Текст, ".", ",")
        Текст = Replace(Текст, "()", "")
        Текст = Replace(Текст, "( - ", "(-")
        Текст = Replace(Текст, "( ", "(")
        Текст = Replace(Текст, " )", ")")

        'Текст = Replace(Текст, "- -", "-")
        'Текст = Replace(Текст, "- ", " -") 'Что это решало? На данный момент оно наоборот ломает..
        'Текст = Replace(Текст, "--", " - -")
        'Текст = Replace(Текст, "---", "-")
        'Блок Открыть: Похоже эта чать не взаимодействует и ненужна, оставлю на всяк случай..
        'Текст = Replace(Текст, "(-(", "((")
        'Текст = Replace(Текст, "(+(", "((")
        'Текст = Replace(Текст, ")-)", "))")
        'Текст = Replace(Текст, ")+)", "))")
        'Блок(Закрыть)

        'Текст = Replace(Текст, "-", " - ")
        'Убираем знаки перехода строки
        Текст = Replace(Текст, Chr(10), "")
        Текст = Replace(Текст, Chr(13), "")
        Фикс = Текст
    End Function

    Public Function CountSpace(ByVal s As String) As Integer
        Dim p As Integer
        p = 0
        For i = 1 To Len(s)
            If Mid(s, i, 1) = " " Then
                p = p + 1
            End If
        Next i
        CountSpace = p
    End Function

    Private Function ПоискНеточн(ByVal s As String) As Boolean
        Dim копия As String
        Dim знаки As Long
        Dim цифры As Long
        s = Replace(s, "mod", "/")
        s = ОтчОтТекста(s)
        копия = s
        s = Фикс(s)
        s = ПравПроб(s)
        s = LCase(s)
        's = Replace(s, "( ", "(")
        's = Replace(s, " )", ")")
        s = Replace(s, " - -", "-")
        s = Replace(s, " + -", "-")
        s = Replace(s, " / -", "-")
        s = Replace(s, " * -", "-")
        s = Replace(s, "(-", "(")
        s = Replace(s, "^ - ", "^")
        s = Replace(s, "sin -", "-")
        s = Replace(s, "cos -", "-")
        s = Replace(s, "tg -", "-")
        s = Replace(s, "ln -", "-")
        s = Replace(s, "mod -", "-")
        s = Replace(s, "exp -", "-")
        s = УдалВсехПроб(s)
        s = ОтчОтЦифр(s)
        знаки = Len(s)
        копия = Фикс(копия)
        копия = ОтчОтЗнаков(копия)
        копия = ПравПроб(копия)
        копия = ПерПроб(копия)
        цифры = Кол(копия)

        If цифры = знаки Then ПоискНеточн = True Else ПоискНеточн = False
    End Function

    Public Function УдалВсехПроб(ByVal s As String)
        s = ПравПроб(s)
        s = Replace(s, " ", "")
        УдалВсехПроб = s
    End Function

    'Считалка количества слов в тексте(Счёт идёт от 0)
    Public Function Кол(ByVal s As String) As Long
        s = ПравПроб(s)
        For i = 1 To Len(s)
            If Mid(s, i, 1) = " " Then
                If Len(s) = i Then
                    s = Microsoft.VisualBasic.Strings.Left(s, Len(s) - 1)
                End If
            End If
        Next i
        Кол = CountSpace(s)
    End Function

    Public Sub KeyA(ByVal text As String)
        'text = Replace(text, "A", "2")
        'text = Replace(text, "B", "5")
        'text = Replace(text, "-", "")
        'text = Replace(text, "|", "-")
        'text = Replace(text, ":", "+")
        'If Голов(text) = "255" And Len(УдалВсехПроб(text)) = 20 Or Голов(text) = "123" And Len(УдалВсехПроб(text)) = 20 Or Голов(text) = "67" And Len(УдалВсехПроб(text)) = 20 Or Голов(text) = "12" And Len(УдалВсехПроб(text)) = 20 Or Голов(text) = "7" And Len(УдалВсехПроб(text)) = 20 Or Голов(text) = "3" And Len(УдалВсехПроб(text)) = 20 Then Licennzed = True
        Licennzed = True
    End Sub

    Public Sub ЛожВРест(ByVal Ключ As String, ByVal Парам As String)
        Dim regSave As Microsoft.Win32.RegistryKey
        regSave = Microsoft.Win32.Registry.LocalMachine.CreateSubKey("software\Tmc\")
        regSave.SetValue(Парам, Ключ)
        regSave.Close()
    End Sub

    Public Function ЧитИзРест(ByVal Парам As String)
        Dim key As Microsoft.Win32.RegistryKey
        key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("software\Tmc\")
        On Error Resume Next
        ЧитИзРест = CType(key.GetValue(Парам), String)
        key.Close()
    End Function

End Module
